#!/bin/bash -e

#############################
# Manages the updates of projects that are pushed to git and used as Maven dependencies in other projects
#
# "-e"
#############################

commit_message="$1"

#############################
# DReNIn
#############################
cd /Users/joemullen/Desktop/SemSubNEW/

git add .
git commit -m "$commit_message"
git push

#############################
# GeneDisease Analysis
#############################
cd /Users/joemullen/Dropbox/GeneDiseaseAnalysis/

git add .
git commit -m "$commit_message"
git push


#############################
# MeSH Manager
#############################
cd /Users/joemullen/Dropbox/MeSHManager/

mvn clean install

mvn install:install-file -Dfile=/Users/joemullen/Dropbox/MeSHManager/target/MeSHManager-1.0-SNAPSHOT.jar -DpomFile=/Users/joemullen/Dropbox/MeSHManager/pom.xml -DlocalRepositoryPath=/Users/joemullen/Dropbox/maven-repo/

cd /Users/joemullen/Dropbox/maven-repo/


#############################
# LLS
#############################
cd /Users/joemullen/Dropbox/LLS/

mvn clean install

mvn install:install-file -Dfile=/Users/joemullen/Dropbox/LLS/target/LLS-1.0-SNAPSHOT.jar -DpomFile=/Users/joemullen/Dropbox/LLS/pom.xml -DlocalRepositoryPath=/Users/joemullen/Dropbox/maven-repo/

cd /Users/joemullen/Dropbox/maven-repo/


#############################
# Push
#############################

git add .
git commit -m "$commit_message"
git push